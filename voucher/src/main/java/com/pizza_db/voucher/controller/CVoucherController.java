package com.pizza_db.voucher.controller;

import java.util.*;

import javax.validation.Valid;

import com.pizza_db.voucher.model.CVoucher;
import com.pizza_db.voucher.repository.IVoucherRepository;
import com.pizza_db.voucher.service.CVoucherService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
@CrossOrigin(value = "*", maxAge = -1)
public class CVoucherController {
    @Autowired
    IVoucherRepository pVoucherRepository;
    @Autowired
    CVoucherService pVoucherService;
    // Lấy danh sách voucher CÓ dùng service.
    @GetMapping("/vouchers")// Dùng phương thức GET
    public ResponseEntity<List<CVoucher>> getAllVouchersByService() {
        try {
            return new ResponseEntity<>(pVoucherService.getVoucherList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }
    // Lấy voucher theo {id} CÓ dùng service
    @GetMapping("/vouchers/{id}")// Dùng phương thức GET
	public ResponseEntity<CVoucher> getCVoucherByIdWithService(@PathVariable("id") long id) {
		CVoucher voucher = pVoucherService.getCVoucherById(id);
		if (voucher != null) {
			return new ResponseEntity<>(voucher, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
    // Tạo MỚI voucher CÓ dùng service sử dụng phương thức POST
    @PostMapping("/vouchers")// Dùng phương thức POST
	public ResponseEntity<Object> createCVoucherWithService(@Valid @RequestBody CVoucher pVoucher) {
		CVoucher voucher = pVoucherService.createCVoucher(pVoucher);
		if(voucher == null) {
			return ResponseEntity.unprocessableEntity().body(" Voucher already exist");
		} else {
			try {
				return new ResponseEntity<>(pVoucherRepository.save(pVoucher), HttpStatus.CREATED);
			} catch (Exception e) {
				System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
				//return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
				return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
			}
		}
	}
    // Sửa/update voucher theo {id} CÓ dùng service, sử dụng phương thức PUT
    @PutMapping("/vouchers/{id}")// Dùng phương thức PUT
	public ResponseEntity<Object> updateCVoucherByIdWithService(@PathVariable("id") long id, @RequestBody CVoucher pVoucher) {
		CVoucher voucher = pVoucherService.updateCVoucherById(id, pVoucher);
		if (voucher != null) {
			try {
				return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Voucher:"+e.getCause().getCause().getMessage());
			}
		} else {
			return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
		}

	}
    // Xoá/delete voucher theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/vouchers/{id}")// Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    // Xoá/delete tất cả voucher KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/vouchers")// Dùng phương thức DELETE
	public ResponseEntity<CVoucher> deleteAllCVouchers() {
		try {
			pVoucherRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
