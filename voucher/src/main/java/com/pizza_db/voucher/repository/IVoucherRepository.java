package com.pizza_db.voucher.repository;

import com.pizza_db.voucher.model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
