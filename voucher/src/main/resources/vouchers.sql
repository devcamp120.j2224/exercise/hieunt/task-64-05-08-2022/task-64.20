-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2022 at 06:31 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

DROP TABLE IF EXISTS `vouchers`;
CREATE TABLE `vouchers` (
  `id` bigint(20) NOT NULL,
  `ghi_chu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ma_voucher` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `phan_tram_giam_gia` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vouchers`
--

INSERT INTO `vouchers` (`id`, `ghi_chu`, `ma_voucher`, `ngay_cap_nhat`, `ngay_tao`, `phan_tram_giam_gia`) VALUES
(1, NULL, '16512', NULL, NULL, '10'),
(2, NULL, '12354', NULL, NULL, '10'),
(3, NULL, '12331', NULL, NULL, '10'),
(4, NULL, '12332', NULL, NULL, '10'),
(5, NULL, '95531', NULL, NULL, '10'),
(6, NULL, '81432', NULL, NULL, '10'),
(7, NULL, '15746', NULL, NULL, '10'),
(8, NULL, '76241', NULL, NULL, '10'),
(9, NULL, '64562', NULL, NULL, '10'),
(10, NULL, '25896', NULL, NULL, '10'),
(11, NULL, '87654', NULL, NULL, '20'),
(12, NULL, '86423', NULL, NULL, '20'),
(13, NULL, '46253', NULL, NULL, '20'),
(14, NULL, '46211', NULL, NULL, '20'),
(15, NULL, '36594', NULL, NULL, '20'),
(16, NULL, '24864', NULL, NULL, '20'),
(17, NULL, '23156', NULL, NULL, '20'),
(18, NULL, '13946', NULL, NULL, '20'),
(19, NULL, '34962', NULL, NULL, '20'),
(20, NULL, '26491', NULL, NULL, '20'),
(21, NULL, '94634', NULL, NULL, '30'),
(22, NULL, '87643', NULL, NULL, '30'),
(23, NULL, '61353', NULL, NULL, '30'),
(24, NULL, '64532', NULL, NULL, '30'),
(25, NULL, '89436', NULL, NULL, '30'),
(26, NULL, '73256', NULL, NULL, '30'),
(27, NULL, '21561', NULL, NULL, '30'),
(28, NULL, '35468', NULL, NULL, '30'),
(29, NULL, '32486', NULL, NULL, '30'),
(30, NULL, '96462', NULL, NULL, '40'),
(31, NULL, '41356', NULL, NULL, '40'),
(32, NULL, '76164', NULL, NULL, '40'),
(33, NULL, '72156', NULL, NULL, '40'),
(34, NULL, '46594', NULL, NULL, '40'),
(35, NULL, '35469', NULL, NULL, '40'),
(36, NULL, '34546', NULL, NULL, '40'),
(37, NULL, '34862', NULL, NULL, '40'),
(38, NULL, '14652', NULL, NULL, '40'),
(39, NULL, '40685', NULL, NULL, '40'),
(40, NULL, '92061', NULL, NULL, '50'),
(41, NULL, '70056', NULL, NULL, '50'),
(42, NULL, '41603', NULL, NULL, '50'),
(43, NULL, '80320', NULL, NULL, '50'),
(44, NULL, '40381', NULL, NULL, '50'),
(45, NULL, '44306', NULL, NULL, '50'),
(46, NULL, '10641', NULL, NULL, '50'),
(47, NULL, '70651', NULL, NULL, '50'),
(48, NULL, '80325', NULL, NULL, '50'),
(51, 'ghi chu tren 50', '1234', NULL, NULL, '65'),
(52, 'ghi chu tren 50', '1235', NULL, NULL, '65'),
(53, 'ghi chu tren 50', '1236', NULL, NULL, '65'),
(54, 'update 1', '87654', '2022-08-05 13:23:34', '2022-08-05 11:31:53', '30'),
(60, 'ok 2', '543210', '2022-08-05 03:12:06', '2022-08-05 03:11:46', '19'),
(61, 'sửa lần 1', '56789', '2022-08-05 14:45:09', '2022-08-05 14:42:14', '21'),
(62, 'tạo mới', '34567', NULL, '2022-08-05 14:45:59', '33'),
(63, 'update lần 1', '35791', '2022-08-05 15:35:07', '2022-08-05 15:24:56', '35');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
